// Library imports
import React, { Component } from 'react';

// Relative Imports
import { Page, Body, Section } from '../constants/layout';
import { Heading, Paragraph } from '../constants/type.js';

const iframe = '<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeOsBj5ZzrkiiPwPbs8TRyShVplwezLj8uN00M3_sqdNQuM-w/viewform?embedded=true" width="760" height="1500" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>';

const iframe2 = '<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSeROqXKRxOfLhF46kpUPAWtJLf6UQfV_oR1PlHgRa7sKux24A/viewform?embedded=true" width="700" height="1120" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>';


class Bounties extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  iframe() {
    return {
      __html: iframe
    }
  }

  iframe2() {
    return {
      __html: iframe2
    }
  }


  render() {
    return (
      <Page>
        <Body>
          <Section>

            <Heading>New Twitter Bounty (scroll down for Telegram bounty)</Heading>
            <Paragraph>
              Retweet this tweet and fill out the form below. https://twitter.com/receiptcoin/status/978712338915184640.
              Tweets with under 500 and over 50 followers get 10 RC and tweets over 500 followers get 20 RC. Referrers get +5 RC
              for tweet they refer. LIMIT ONE TWEET PER PERSON. UNLIMITED VALID REFERRALS.
            </Paragraph>

            <div dangerouslySetInnerHTML={ this.iframe() } />

            <Heading>Join our telgram https://t.me/ReceiptCoin and remain until June 1st. You must be in the channel on June 1st in order to receive the reward.
            The Telegram code is in the pinned message.</Heading>

            <div dangerouslySetInnerHTML={ this.iframe2() } />

          </Section>
        </Body>


      </Page>
    );
  }
}

export default Bounties;
