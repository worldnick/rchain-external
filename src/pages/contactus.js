// Library imports
import React, { Component } from 'react';

// Relative Imports
import { Page, Body, Section } from '../constants/layout';
import { Heading, Paragraph } from '../constants/type.js';


class ContactUs extends Component {
  componentDidMount() {
    window.scrollTo(0, 0);
  }

  render() {
    return (
      <Page>
        <Body>
          <Section>
            <Paragraph>

              <div id="zbwid-3abbd348"></div>
              <div>If you cannot see this form please turn off your adblocker.</div>
            </Paragraph>
          </Section>
        </Body>

      </Page>
    );
  }
}

export default ContactUs;
