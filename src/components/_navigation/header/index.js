// Library imports
import React, { Component } from 'react';

// Relative Imports
import {
  Container,
  Brand,
  Company,
  Icon,
  Button,
  ButtonContainer,
  Item,
  Hamburger,
  Menu,
  Row
} from './styles';
import menu from '../../../assets/icons/menu.svg';
import Mobile from '../mobile';
import Logo from '../../../assets/icons/RC-logo-small.png';

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      mobileMenu: false
    };
  }

  toggleMenu = () => {
    this.setState(prevState => ({
      mobileMenu: !prevState.mobileMenu
    }));
  };

  render() {
    return (
      <Container>
        <Row>
          <Brand to="/">
            <Icon src={Logo} />
            <Company>ReceiptChain</Company>
          </Brand>
          <ButtonContainer>
            <Item to="/">Home</Item>
            <Item to="/whitepaper">Whitepaper</Item>
            <Item to="/team">Team</Item>
            <Item to="/faq">F.A.Q</Item>
            <a style={{"color": "rgba(255, 255, 255, 0.4)",
            "font-size": "14px",
            "margin-left": "8px",
            "margin-right": "8px",
            "text-decoration": "none",
            "font-family": "sans-serif",
            "text-transform": "uppercase"}} href="https://play.google.com/store/apps/details?id=com.receiptcoin.receiptchain" target="new">Download Android</a>

            <Button to="/subscribe">Subscribe</Button>
          </ButtonContainer>

          <Hamburger onClick={this.toggleMenu}>
            <Menu src={menu} />
          </Hamburger>
        </Row>
        {this.state.mobileMenu && <Mobile onClick={this.toggleMenu} />}
      </Container>
    );
  }
}

export default Header;
