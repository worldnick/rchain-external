import styled from 'styled-components';
import { _blue } from '../../../constants/colors';
import media from '../../../constants/media';
import { Link } from 'react-router-dom';
import ReactPlayer from 'react-player';

export const Container = styled.div`
  width: 100vw;
  height: 60vh;
  background: ${_blue};
  display: flex;
  justify-content: center;
  flex-direction: row;
  align-items: center;
  ${media.mobile`
    flex-direction: column;
    height: 80vh;
  `};
  ${media.tablet`
    flex-direction: column;
    height: 80vh;
  `};
`;

export const Video = styled.div`
  width:90%;
  height:50%;
  position: relative;
  display: flex;
  background: clear;
  justify-content: center;
  flex-direction: row;
  padding: 12px;
`;

export const Microcopy = styled.div`
  max-width: 992px;
  height: 55%;
  width: 90%;
  display: flex;
  background: clear;
  justify-content: center;
  flex-direction: column;
  padding: 12px;

  ${media.mobile`
    margin: 16px;
  `};
`;

export const Heading = styled.div`
  font-size: 44px;
  color: white;
  font-family: 'Open Sans', sans-serif;
  font-weight: bold;
  margin: 8px;
  ${media.mobile`
    font-size: 20px;
  `};
  ${media.tablet`
    font-size: 30px;
  `};
`;

export const SubHeading = styled.div`
  font-size: 16px;
  color: white;
  font-family: 'Sintony', sans-serif;
  font-weight: 400;
  margin: 8px;

  ${media.mobile`
    width: 100%;
    font-size: 12px;
  `};

  ${media.tablet`
    width: 100%;
    max-width: 500px;
    font-size: 12px;

  `};
`;

export const Row = styled.div`
  height: auto;
  width: auto;
  display: flex;
  flex-direction: row;
  margin: 8px;

  ${media.tablet`
    flex-direction: column;
  `};
`;

export const Fill = styled.div`
  height: 48px;
  width: 182px;
  font-size: 16px;
  background: white;
  color: ${_blue};
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  margin: 4px;
  font-family: 'Sintony', sans-serif;

  &:hover {
    cursor: pointer;
  }
`;

export const Outline = styled(Link)`
  height: 48px;
  width: auto;
  font-size: 16px;
  background: ${_blue};
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 4px;
  margin: 4px;
  border: 1px solid white;
  font-family: 'Sintony', sans-serif;
  padding-left: 12px;
  padding-right: 12px;
  text-decoration: none;

  &:hover {
    cursor: pointer;
  }
`;

export const aLink = {
  height: '48px',
  minHeight: '48px',
  width: 'auto',
  fontSize: '16px',
  color: 'white',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: '4px',
  margin: '4px',
  border: '1px solid white',
  fontFamily: 'Sintony, sans-serif',
  paddingLeft: '12px',
  paddingRight: '12px',
  textDecoration: 'none',
  cursor: 'pointer',
  }
;

export const Play = styled(ReactPlayer)`

`;
