// Library imports
import React from 'react';

// Relative Imports
import {
  Container,
  Microcopy,
  Heading,
  SubHeading,
  Outline,
  Row,
  aLink,
  Video,
  Play
} from './styles';
import Countdown from '../../_misc/countdown';

const Hero = () => {
  return (
    <Container>
      <Video>
        <Play
          url="https://youtu.be/ss8DA5_BTro"
          className="react-player"
          width="100%"
          height="100%"
        />
      </Video>
      <Microcopy>
        <Heading>ReceiptCoin for sale on ERCdEX</Heading>
        <SubHeading>
          Our new V2 Android app is in beta. While you can get 10 free RC for downloading and installing the app you can buy more on ERCdEX and import it into the app.
        </SubHeading>
        <Row>
          <a style={aLink} href="https://www.ercdex.com" target="new">Buy ReceiptCoin</a>
          <a style={aLink} href="https://play.google.com/store/apps/details?id=com.receiptcoin.receiptchain" target="app">Download the App</a>
        </Row>
      </Microcopy>
    </Container>
  );
};

export default Hero;
