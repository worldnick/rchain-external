// Library imports
import React, { Component } from 'react';

// Relative Imports
import { Container, Presenter, Body, Phone, Panel, Section } from './styles';
import SectionHeader from '../../_misc/section-header';
import iPhone from '../../../assets/img/inventory.png';
import { Paragraph, Title } from '../../../constants/type.js';

class Features extends Component {
  render() {
    return (
      <Container>
        <SectionHeader title="ReceiptChain App Features" />
        <Presenter>
          <Body>
            <Panel>
              <Section>
                <Title>Track Your Things</Title>
                <Paragraph>
                  Every time we sell a product the images, description and meta
                  data for that product are lost forever. With our files on the blockchain
                  the data survives. This means less photoshoots,
                  looking for lost manuals, and trying to write catchy
                  descriptions each time things change hands. This data goes with the product from
                  manufacturer to reseller to customer to customer.
                </Paragraph>
              </Section>
              <Section>
                <Title>Authentication</Title>
                <Paragraph>
                  ReceiptCoin helps consumers and businesses create digital
                  assets that survive sale and trading. These assets represent
                  the digital identity of an object and can be used
                  interoperably between parties or internally to track and
                  accumulate historical records of objects such as location and
                  owner. If you are a vendor you can register a key with ReceiptCoin to
                  give your customers mathematical proof that it started with you.
                </Paragraph>
              </Section>
              <Section>
                <Title>Proof of Existence</Title>
                <Paragraph>
                  We can take a digital fingerprint of anything, an image, song, video,
                  document, or anything you would like to prove existed at
                  any given time. A picture at a traffic accident or an image of a rare antique
                  works perfectly with our system. Once the digital fingerprint is in the blockchain
                  only your original file will satisfy that fingerprint proving that digital file existed
                  at that time in the history of the blockchain. This means you can prove anything existed
                  at any time.
                </Paragraph>
              </Section>
              <Section>
                <Title>Organization and Personal Inventory</Title>
                <Paragraph>
                  With ReceiptCoin&apos;s app and search engine you will be able to track
                  the location of anything in the world on the blockchain. You will be able
                  to add your objects to public search as available for sale or keep
                  your information encrypted. It&apos;s up to you. Keep track of what you have
                  and sell what you don&apos;t.
                </Paragraph>
              </Section>
            </Panel>
            <Phone src={iPhone} />
          </Body>
        </Presenter>
      </Container>
    );
  }
}

export default Features;
