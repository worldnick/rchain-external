// Library imports
import React from 'react';

// Relative Imports
import { Container, Heading, SubHeading, Outline, Row } from './styles';
import { Body } from '../../../constants/layout.js';

const Overview = () => {
  return (
    <Container>
      <Body>
        <Heading>Whitepaper</Heading>
        <SubHeading>

        </SubHeading>
        <Row>
          <Outline
            href="https://firebasestorage.googleapis.com/v0/b/receiptcoin-receiptchain-de560.appspot.com/o/ReceiptCoin-Whitepaper-june1.pdf?alt=media&token=251c2a3b-1548-424c-b734-5cd37b7239b7"
            download>
            Whitepaper Download - English
          </Outline>
        </Row>
      </Body>
    </Container>
  );
};

export default Overview;
