// Library imports
import React, { Component } from 'react';

// Relative Imports
import { Container } from './styles.js';
import { Body, Section } from '../../../constants/layout';
import { Heading, Paragraph } from '../../../constants/type';

class Overview extends Component {
  render() {
    return (
      <Container>
        <Body>
        <Section>
          <Heading>
            Is this still ReceiptCoin?
          </Heading>
          <Paragraph>
            ReceiptCoin project is now ReceiptChain, but the token is still ReceiptCoin with the same contract.
          </Paragraph>
        </Section>
        <Section>
          <Heading>
            How do I use the tokens in the app?
          </Heading>
          <Paragraph>
            You will have to import your ERC20 ReceipCoins into the app to use them on the ReceiptChain blockchain.
          </Paragraph>
        </Section>
        <Section>
          <Heading>
            Will you do a public sale?
          </Heading>
          <Paragraph>
            Due to the regulatory environment we will not be selling tokens from a crowdsale.
          </Paragraph>
        </Section>
        <Section>
          <Heading>
            How can I get tokens?
          </Heading>
          <Paragraph>
            You will still have an opportunity to get free tokens in the app.
          </Paragraph>
        </Section>
          <Section>
            <Heading>
              How will I see my tokens?
            </Heading>
            <Paragraph>
              In some apps like Delta (RCT), and imToken (RC) you will see your
              tokens immediately. In others like MyEtherWallet and Ethereum
              Wallet you will need to add the token in the custom token section.
              The token watch address is
              0xd6e49800dECb64C0e195F791348C1e87a5864FD7
            </Paragraph>
          </Section>
          <Section>
            <Heading>
              What is the token contract address
            </Heading>
            <Paragraph>
              0xd6e49800dECb64C0e195F791348C1e87a5864FD7
            </Paragraph>
          </Section>
          <Section>
            <Heading>How many decimals is ReceiptCoin</Heading>
            <Paragraph>9</Paragraph>
          </Section>
          <Section>
            <Heading>What is the symbol for ReceiptCoin </Heading>
            <Paragraph>
              On Delta iOS app we are RCT. On imToken we are RC(ReciptCoin). On
              MyEtherWallet we are RC. What matters is that your root contract is 0xd6e49800dECb64C0e195F791348C1e87a5864FD7
            </Paragraph>
          </Section>
          <Section>
            <Heading>Is ReceiptCoin a security?</Heading>
            <Paragraph>
               ReceiptCoin is the base utility coin for the ReceiptChain blockchain like Ether is for Ethereum. It is necessary for the chain to function.
               ReceiptCoin is used to purchase the service of adding and tracking property data to the ReceiptChain blockchain. The secondary value of ReceiptCoin is not guaranteed by the company. There are no voting rights or dividends.
            </Paragraph>
          </Section>
        </Body>
      </Container>
    );
  }
}

export default Overview;
