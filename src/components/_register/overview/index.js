// Library imports
import React, { Component } from 'react';

// Relative Imports
import { Container } from './styles.js';
import { Body } from '../../../constants/layout';
import Auth from '../../_auth/auth';
import Input from '../../_inputs/input';

class Overview extends Component {
  render() {
    return (
      <Body>
        <Container>
          <Auth
            title="Subscribe for Updates"
            subtitle="">
            <Input label="Email" type="email" name="email" class="required email" placeholder="Enter your email" />
          </Auth>
        </Container>
      </Body>
    );
  }
}

export default Overview;
