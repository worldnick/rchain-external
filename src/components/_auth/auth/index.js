// Library imports
import React, { Component } from 'react';

// Relative Imports
import { Form, Header, Title, Subtitle, Body, Footer, Submit } from './styles';

const displayNone = {
  display:'none'
}

const offScreen = {
  position: 'absolute',
  left: '-5000px'
}

class Auth extends Component {
  render() {
    const { title, subtitle, children, onSubmit } = this.props;
    return (
      <Form onSubmit={onSubmit} method="post" class="validate" target="_blank" action="https://us-central1-receiptchain-dev.cloudfunctions.net/app/emailSignup/">
        <Header>
          <Title>{title}</Title>
          <Subtitle>{subtitle}</Subtitle>
        </Header>
        <Body>{children}</Body>
        <Footer>
          <Submit type="submit" value="Subscribe" name="subscribe">Submit</Submit>
        </Footer>
      </Form>
    );
  }
}

export default Auth;
